const { UPLOAD_DIR } = require('./config');
const ensureDirectoryExistence = require('./helper');
const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');



router.post('/', (req, res) => {
  const { filename, content } = req.body;

  if (content === undefined) {
    return res.status(400).json({ message: `Please specify 'content' parameter` });
  }

  if (filename === undefined) {
    return res.status(400).json({ message: `Please specify 'filename' parameter` });
  }

  const extension = path.extname(filename);

  const isAllowedExtension = /^.*\.(log|txt|json|yaml|xml|js)$/gmi.test(extension);

  if (!filename || !isAllowedExtension) {
    return res.status(400).json({ message: `Disallowed file extension '${extension}'` });
  }

  const filePath = path.join(__dirname + UPLOAD_DIR, filename);
  ensureDirectoryExistence(filePath);

  fs.writeFile(filePath, content, function (err) {
    if (err) {
      console.log(err);
      return res.status(500).json({ message: `Server error` });
    } else {
      return res.status(200).json({ message: `File created successfully` });
    }
  });

});




router.get('/', (req, res) => {

  const isParametersPassed = Object.keys(req.body).length !== 0;
  if (isParametersPassed) {
    return res.status(400).json({ message: `Client error. No parameters needed` });
  }

  fs.readdir(__dirname + UPLOAD_DIR, (err, files) => {
    if (err) {
      console.log(err);
      return res.status(400).json({ message: `No files uploaded yet` });
    }

    return res.status(200)
      .json({
        message: `Success`,
        files: files
      });

  });
});




router.get('/:filename', (req, res) => {
  const filePath = path.join(__dirname + UPLOAD_DIR, req.params.filename);

  fs.access(filePath, fs.F_OK, (err) => {

    if (err) {
      console.error(err)
      return res.status(400).json({ message: `No file with '${req.params.filename}' filename found` });
    }

    fs.stat(filePath, (err, stats) => {

      if (err) {
        console.log(err);
        return res.status(500).json({ message: `Server error` });
      }

      fs.readFile(filePath, 'utf8', (err, data) => {

        if (err) {
          console.log(err);
          return res.status(500).json({ message: `Server error` });
        }

        return res.status(200)
          .json({
            message: `Success`,
            filename: `${req.params.filename}`,
            content: data,
            extension: path.extname(req.params.filename).substring(1),
            uploadedDate: stats.birthtime,
          });

      });
    });
  });
});





module.exports = router;