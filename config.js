module.exports = {

  PORT: 8080,
  API_PATH: '/api/files',
  LOG_DIR: '/log',
  UPLOAD_DIR: '/upload',

}
