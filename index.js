const { PORT, API_PATH, LOG_DIR } = require('./config');
const ensureDirectoryExistence = require('./helper');
const apiRouter = require('./apiRouter');
const express = require('express');
const morgan = require('morgan');
const path = require('path');
const fs = require('fs');
const app = express();


app.use(express.json()) // for parsing application/json


const logPath = path.join(__dirname + LOG_DIR, 'access.log');
ensureDirectoryExistence(logPath); // create dir
const accessLogStream = fs.createWriteStream(logPath, { flags: 'a' });
app.use(morgan('combined', { stream: accessLogStream })); // write log


app.use(API_PATH, apiRouter); // router

app.all('*', (req, res) => {
  return res.status(400).json({ message: `Bad request.` });
})


app.use((err, req, res, next) => {
  if (err) {
    console.error(err.stack);
    return res.status(500).json({ message: `Server error` });
  }
});

app.listen(PORT, () => {
  console.log(`Server works at port ${PORT}`);
});
